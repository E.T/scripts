#!/bin/bash

while getopts ":nfdh" opt; do
  case ${opt} in
    n )
        lsblk -d
        printf "$(tput setaf 2)Choose your disk: $(tput sgr 0)"
        read disk
        if [ -e /dev/$disk ]
        then
          printf "$(tput setaf 2)/dev/$disk exist$(tput sgr 0)"
          printf "\n"
        else
          printf "$(tput setaf 2)/dev/$disk doesn't exist$(tput sgr 0)"
          printf "\n"
          exit 1
        fi
        sudo umount /dev/$disk*
        sudo wipefs -af /dev/$disk
        (
        echo o
        sleep 1
        echo n
        sleep 1
        echo p
        sleep 1
        echo 1
        sleep 1
        echo
        sleep 1
        echo
        sleep 1
        echo w
        ) | sudo fdisk /dev/$disk
        sudo mkfs.ntfs /dev/$disk'1'
      ;;
    f )
        lsblk -d
        printf "$(tput setaf 2)Choose your disk: $(tput sgr 0)"
        read disk
        if [ -e /dev/$disk ]
        then
          printf "$(tput setaf 2)/dev/$disk exist$(tput sgr 0)"
          printf "\n"
        else
          printf "$(tput setaf 2)/dev/$disk doesn't exist$(tput sgr 0)"
          printf "\n"
          exit 1
        fi
        sudo umount /dev/$disk*
        sudo wipefs -af /dev/$disk
        (
        echo o
        sleep 1
        echo n
        sleep 1
        echo p
        sleep 1
        echo 1
        sleep 1
        echo
        sleep 1
        echo
        sleep 1
        echo w
        ) | sudo fdisk /dev/$disk
        sudo mkfs.vfat -F 32 /dev/$disk'1'
      ;;
    d )
        lsblk -d
        printf "$(tput setaf 2)Choose your disk: $(tput sgr 0)"
        read disk
        if [ -e /dev/$disk ]
        then
          printf "$(tput setaf 2)/dev/$disk exist$(tput sgr 0)"
          printf "\n"
        else
          printf "$(tput setaf 2)/dev/$disk doesn't exist$(tput sgr 0)"
          printf "\n"
          exit 1
        fi
        sudo umount /dev/$disk*
        printf "$(tput setaf 2)Choose your blocksize, the smaller the number the longer it takes$(tput sgr 0)"
        printf "\n"
        printf "$(tput setaf 2)Number to choose from are 1, 2, 3, 4 and 5$(tput sgr 0)"
        printf "\n"
        printf "$(tput setaf 2)Choose your blocksize: $(tput sgr 0)"
        read level
        if [ $level = 1 ]
        then
          bs=1
        elif [ $level = 2 ]
        then
          bs=512
        elif [ $level = 3 ]
        then
          bs=1k
        elif [ $level = 4 ]
        then
          bs=1M
        elif [ $level = 5 ]
        then
          bs=4M
        else
          echo "Invalid number"
          exit 1
        fi
        sudo dd if=/dev/urandom of=/dev/$disk status=progress oflag=sync bs=$bs
      ;;
    h )
        echo "[-n] for ntfs (ERASES DISK)"
        echo "[-f] for fat32 (ERASES DISK)"
        echo "[-d] for dd (ERASES DISK)"
        echo "[-h] for help"
      ;;
    \? )
        echo -e "Usage: disk [-n] [-f] [-d]\nFor help: [-h]" 1>&2
        exit 1
      ;;
  esac
done
if [ $OPTIND -eq 1 ]; then echo "No options were passed, use [-h] flag for help"; fi
shift $((OPTIND -1))
