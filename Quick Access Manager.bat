@ECHO OFF
title Quick Access
:ZERO
echo 1: Create a Quick access backup to (C:\drivers\quick_access_folder)
echo 2: Restore Quick access from backup in (C:\drivers\quick_access_folder)
echo 3: Create a Quick access backup and DELETE the current one in use.
echo 4: Delete the Quick access backup
echo 5: Delete the CURRENT Quick access in use
echo 6: Quit the script
CHOICE /C 123456 /N
if %errorlevel%==6 GOTO SIX
if %errorlevel%==5 GOTO FIVE
if %errorlevel%==4 GOTO FOUR
if %errorlevel%==3 GOTO THREE
if %errorlevel%==2 GOTO TWO
if %errorlevel%==1 GOTO ONE
pause
exit
:ONE
if not exist "C:\drivers\quick_access_folder" mkdir C:\drivers\quick_access_folder
xcopy /s /v %AppData%\Microsoft\Windows\Recent\AutomaticDestinations C:\drivers\quick_access_folder
pause
exit
:TWO
if not exist "C:\drivers\quick_access_folder" echo No backups made to C:\drivers\quick_access_folder
xcopy /s /v /y C:\drivers\quick_access_folder %AppData%\Microsoft\Windows\Recent\AutomaticDestinations
pause
exit
:THREE
if not exist "C:\drivers\quick_access_folder" mkdir C:\drivers\quick_access_folder
xcopy /s /v %AppData%\Microsoft\Windows\Recent\AutomaticDestinations C:\drivers\quick_access_folder
del /F /Q %APPDATA%\Microsoft\Windows\Recent\AutomaticDestinations\*
pause
GOTO ZERO
exit
:FOUR
RD /S /Q "C:\drivers\quick_access_folder"
pause
GOTO ZERO
exit
:FIVE
RD /S /Q "%AppData%\Microsoft\Windows\Recent\AutomaticDestinations"
pause
GOTO ZERO
exit
:SIX
echo Quitting script
pause
exit